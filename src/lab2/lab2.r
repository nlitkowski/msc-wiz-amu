library(ggplot2)
library(PogromcyDanych)
library(dplyr)

setLang(lang = 'eng')


# task 1 ------------------------------------------------------------------

cats_birds %>%
  ggplot(aes(x = length, y = speed, shape = group, color = group)) +
  geom_point(size = 3) + 
  geom_smooth(method = 'lm', se = FALSE, fullrange = TRUE) +
  xlab('Length [m]') + 
  ylab('Speed  [km/h]') +
  guides(color = guide_legend(title = 'Group of animals')) +
  ggtitle('Dependency between length and speed of cats and birds') +
  theme_classic()

# task 2 ------------------------------------------------------------------

pearson %>%
  ggplot(aes(x = father, y = son)) +
  geom_point(size = 1) +
  geom_smooth(method = 'lm', se = FALSE, fullrange = TRUE) +
  xlab('Father height') + 
  ylab('Son height') +
  ggtitle('Dependency between father and son height') +
  theme_classic()

# task 3 ------------------------------------------------------------------

n.series <- 20

set.seed(1000)

seriesIMDB %>%
  pull(series) %>% # wyciagam kolumne series
  unique() %>% # kazdy serial raz
  as.vector() %>% # wektor nazw
  sample(n.series, replace = FALSE) -> sel.series # replace bez powtórzen

seriesIMDB %>%
  filter(series %in% sel.series) -> df # filtruje          seriale
  
df %>%
  mutate(series = reorder(series, note, median)) %>%
  ggplot(aes(y = series, x = note, group = series)) + 
  geom_boxplot(fill  = 'lightgreen') +
  theme_classic()

# task 4 ------------------------------------------------------------------

diagnosis %>%
  na.omit() %>%
  select(eduk4_2013, gp29) %>%
  ggplot(aes(x = eduk4_2013, fill = gp29)) +
  geom_bar() +
  coord_flip() +
  theme_classic()

# task 5 ------------------------------------------------------------------

auta2012 %>%
  filter(Brand == 'Volkswagen', Model == 'Passat') %>%
  as_tibble() %>%
  select(Year, Price.in.PLN) %>%
  ggplot(aes(x = Year, y = Price.in.PLN)) +
  geom_smooth(se = FALSE) +
  labs(x = 'Rok produckji', y = 'Cena [PLN]') +
  theme_classic()
