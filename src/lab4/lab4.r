library(ggplot2)
library(sf) # Spatial data format
library(rnaturalearth) # Provides a map of countries of the entire world
library(ggspatial) # Scale bar and North arrow
library(maps) # USA
library(googleway) # Access to Google Maps APIs,
library(ggrepel) # Flexible approach to deal with label placement
library(cowplot) # Grid plots
library(ggmap) # Google maps

theme_set(theme_bw()) # Theme appropriate for maps

world <- ne_countries(scale = 'medium', returnclass = 'sf') # Data
world_points <- cbind(world, st_coordinates(st_centroid(world$geometry)))

# Zadanie 1 ---------------------------------------------------------------
# Przygotuj mapę basenu Morza Śródziemnego (podobna do mapy Zatoki Meksykańskiej z Laboratorium).

ggplot(data = world) + 
  geom_sf(fill= 'antiquewhite') + 
  geom_text(data = world_points, 
            aes(x = X, y = Y, label = name), 
            color = 'darkblue', 
            fontface = 'bold', check_overlap = FALSE) + 
  annotate(geom = 'text', 
           x = 20, 
           y = 34, 
           label = 'Mediterranean sea', 
           fontface = 'italic', 
           color = 'grey22',
           size = 6) + 
  annotation_scale(location = 'bl', 
                   width_hint = 0.5) + 
  annotation_north_arrow(location = 'bl', 
                         which_north = 'true', 
                         pad_x = unit(0.75, 'in'), 
                         pad_y = unit(0.5, 'in'), 
                         style = north_arrow_fancy_orienteering) + 
  coord_sf(xlim = c(4.93, 30.93), 
           ylim = c(21.73, 47.73),
           expand = FALSE) + 
  xlab('Longitude') + 
  ylab('Latitude') + 
  ggtitle('Map of the Mediterrranean sea') + 
  theme(panel.grid.major = element_line(color = gray(0.5), 
                                        linetype = 'dashed', 
                                        size = 0.5), 
        panel.background = element_rect(fill = 'aliceblue'))

# Zadanie 2 ---------------------------------------------------------------
# Przygotuj mapę Alaski (podobna do mapy Florydy z Laboratorium).
(sites <- data.frame(longitude = c(-80.144005, -80.109), latitude = c(26.479005, 
                                                                      26.83))) # Data for points

(sites <- st_as_sf(sites, coords = c("longitude", "latitude"), 
                   crs = 4326, agr = "constant")) # Change format
states <- st_as_sf(map("state", plot = FALSE, fill = TRUE))
head(states)

states <- cbind(states, st_coordinates(st_centroid(states)))

states$ID <- tools::toTitleCase(as.character(states$ID)) # Capital letter at the begining
head(states)
states$nudge_y <- -1
states$nudge_y[states$ID == "Florida"] <- 0.5
states$nudge_y[states$ID == "South Carolina"] <- -1.5

counties <- st_as_sf(map("county", plot = FALSE, fill = TRUE))
counties <- subset(counties, grepl("florida", counties$ID))
counties$area <- as.numeric(st_area(counties))
head(counties)

key <- "AIzaSyDwvrwP8k8dr5LBv34Wal1PAu6L2Te9FxM" # Real key needed!!!; Directions API, Geocoding API, Geolocation API, Maps Static API shoud be enabled
flcities <- data.frame(state = rep("Florida", 5), 
                       city = c("Miami", "Tampa", "Orlando", "Jacksonville", "Sarasota"))
get.google.coord <- function(x, key) {
  google_geocode(address = paste(x['city'], x['state'], sep = ', '), 
                 key = key)
}

coords <- apply(flcities, 1, get.google.coord, key = key)


flcities <- cbind(flcities, do.call(rbind, lapply(coords, geocode_coordinates)))
(flcities <- st_as_sf(flcities, coords = c("lng", "lat"), remove = FALSE, 
                      crs = 4326, agr = "constant"))
ggplot(data = world) +
  geom_sf(fill = "antiquewhite1") +
  geom_sf(data = counties, aes(fill = area)) +
  geom_sf(data = states, fill = NA) + 
  geom_sf(data = sites, size = 4, shape = 23, fill = "darkred") +
  geom_sf(data = flcities) +
  geom_text_repel(data = flcities, aes(x = lng, y = lat, label = city), 
                  fontface = "bold", nudge_x = c(1, -1.5, 2, 2, -1), nudge_y = c(0.25, 
                                                                                 -0.25, 0.5, 0.5, -0.5)) +
  geom_label(data = states, aes(X, Y, label = ID), size = 5, fontface = "bold", 
             nudge_y = states$nudge_y) +
  scale_fill_viridis_c(trans = "sqrt", alpha = .4) +
  annotation_scale(location = "bl", width_hint = 0.4) +
  annotation_north_arrow(location = "bl", which_north = "true", 
                         pad_x = unit(0.75, "in"), pad_y = unit(0.5, "in"),
                         style = north_arrow_fancy_orienteering) +
  coord_sf(xlim = c(-88, -78), ylim = c(24.5, 33), expand = FALSE) +
  xlab("Longitude") + ylab("Latitude") +
  ggtitle("Observation Sites", subtitle = "(2 sites in Palm Beach County, Florida)") +
  theme(panel.grid.major = element_line(color = gray(0.5), linetype = "dashed", 
                                        size = 0.5), panel.background = element_rect(fill = "aliceblue"))

# Zadanie 3 ---------------------------------------------------------------
# Przygotuj mapę Danii wraz z Grenlandią (podobna do mapy USA z Laboratorium).


gd <- subset(world, sovereignt == "Denmark")
(greenland <- ggplot(data = gd) +
    geom_sf(fill = "cornsilk") +
    coord_sf(crs = st_crs(2217), xlim = c(-190000, 1500000)))

(den <- ggplot(data=gd) +
    geom_sf(fill = "cornsilk") +
    coord_sf(crs = st_crs(4095),xlim = c(150000, 900000),
             ylim = c(1000000, 1500000), expand=FALSE, datum=NA))

ratio <- (1500000 - 1000000) / (900000 - 150000) 

ggdraw(greenland) +
  draw_plot(den, width = 0.26, height = 0.26 * 10/6 * ratio, 
            x = 0.05, y = 0.05)
  