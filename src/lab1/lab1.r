# zad 1 -------------------------------------------------------------------

# zad 7 -------------------------------------------------------------------

# opis danych
# https://stat.ethz.ch/R-manual/R-devel/library/MASS/html/survey.html

# pobieram dane
df <- MASS::survey

# tworze zmienną do tworzenia tabelki
var.temp <- factor(df$Exer,
                   ordered=TRUE,
                   levels = c('None', 'Some', 'Freq'))
table(var.temp)

barplot(table(var.temp), col = 1:3, main = 'Wykres słupkowy')

pie(table(var.temp), col = 5:7, main = 'Wykres kołowy')

# zad 8 -------------------------------------------------------------------

df <- DAAG::orings

par(mfrow=c(1,2))
plot(Total ~ Temperature,
     data = df,
     pch = 20,
     cex = 2)
plot(Total ~ Temperature,
     data = df[c(1,2,4,11,13,18), ],
     pch = 20,
     cex = 2)

par(mfrow = c(1,1))
plot(Total ~ Temperature,
     data = df,
     pch = 20,
     cex = 2)
with(df[c(1,2,4,11,13,18), ],
     points(Temperature, Total,
             col = 'red',
             pch = 20,
             cex = 2))
legend('topright',
       pch = 20,
       col = 1:2,
       cex = 2,
       legend = c('Removed', 'Used'))

# zad 9 -------------------------------------------------------------------

